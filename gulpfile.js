var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var connect = require('gulp-connect');

gulp.task('sass', function () {
    return gulp
        .src('src/sass/index.sass')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(rename('style.css'))
        .pipe(gulp.dest('dest/css'))
});

gulp.task('copyImages', function () {
    return gulp.src('src/images/**/**.*')
        .pipe(gulp.dest('dest/images'));
});

gulp.task('copyFonts', function () {
    return gulp.src('src/fonts/**.*')
        .pipe(gulp.dest('dest/fonts'));
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.*', gulp.series('sass'));
    gulp.watch('src/images/**/*.*', gulp.series('copyImages'));
    gulp.watch('src/fonts/**/*.*', gulp.series('copyFonts'));
});

gulp.task('serve', function() {
    connect.server();
});

gulp.task('dev', gulp.parallel('sass', 'watch', 'copyImages', 'copyFonts', 'serve'));
