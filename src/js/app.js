requirejs.config({
    'baseUrl': 'src/js',
    'paths': {
        'jquery': 'lib/jquery',
        'detectmobilebrowser': 'lib/detectmobilebrowser',
        'flatpickr': 'lib/flatpickr',
        'date': 'lib/date'
    }
});

define([
    'jquery',
    'components/filter-item',
    'components/filter-tag',
    'components/user-table',
    'detectmobilebrowser',
    'date'
], function ($, FilterItem, FilterTag) {
    var $filterTags = $('.js-table-applied-filters');
    var $filterTagsEmpty = $('.js-table-applied-empty');
    var $filterTagsClear = $('.js-table-applied-clear');

    var allFilters = [];

    // add filter in search box and filter tag near to the table
    function createFilterRow () {
        var filterTag = new FilterTag({
            container: $filterTags,
            onClick: function () {
                filter.destroy();
            }
        });

        var filter = new FilterItem({
            onColumnTypeChange: function (select) {
                var text = $('option:selected', select).text();
                filterTag.setTitle(text || 'Column');
            },
            onRemove: function () {
                var index = allFilters.indexOf(filter);
                if (index > -1) {
                    allFilters.splice(index, 1);
                }
                filterTag.destroy();
                updateFilterTagsState();
            }
        });
        allFilters.push(filter);
        updateFilterTagsState();
    }

    function updateFilterTagsState () {
        if ($filterTags.find('.table-nav__filter-item').length === 0) {
            $filterTagsClear.addClass('table-nav__filter-clear--hide');
            $filterTagsEmpty.removeClass('table-nav__filter-empty--hide');
        } else {
            $filterTagsClear.removeClass('table-nav__filter-clear--hide');
            $filterTagsEmpty.addClass('table-nav__filter-empty--hide');
        }
    }

    // create default row
    createFilterRow();

    $filterTagsClear.on('click', function (event) {
        event.preventDefault();
        while(allFilters.length) {
            var filter = allFilters.pop();
            filter.destroy();
        }
        allFilters = [];
    });

    // add new row
    $('.js-search-box-add-row').on('click', function (event) {
        event.preventDefault();
        createFilterRow();
    });

    // pagionation dropdown
    $('.js-pagination-rows, .js-user-table-action').dropdown();

});
