define(['jquery'], function ($) {

    var tagTemplate = [
        '<div class="table-nav__filter-item">',
            '<button class="button button--gray">',
                '<i class="table-nav__filter-times-icon icon icon--times"></i>',
                '<span class="table-nav__filter-title js-filter-tag-text"></span>',
            '</button>',
        '</div>'
    ].join('');

    function FilterTag (options) {
        this.settings = $.extend({
            onClick: $.noop
        }, options);
        this._init();
    }

    $.extend(FilterTag.prototype, {
        _init: function () {
            this.$tag = $(tagTemplate);
            this.$title = this.$tag.find('.js-filter-tag-text');
            this.$title.text(this.settings.title || 'Column');
            if (this.settings.container) {
                this.$tag.appendTo(this.settings.container);
            }
            this.$tag.on('click', this.settings.onClick.bind(this));
        },
        setTitle: function (title) {
            this.$title.text(title);
        },
        destroy: function () {
            this.$tag.off('click');
            this.$tag.remove();
        }
    });

    return FilterTag;
});
