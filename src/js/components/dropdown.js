define(['jquery', 'flatpickr'], function ($, Flatpickr) {

    var defaults = {
        checkAll: 'Check all',
        unCheckAll: 'Uncheck all'
    };
    function Plugin (element, options) {
        this.element = element;
        this.$el = $(element);
        this.settings = $.extend({}, defaults, options);
        this._init();
    }

    $.extend(Plugin.prototype, {
        _init: function () {

            this._linkClickHandler = this._linkClickHandler.bind(this);
            this._windowOnClickHandler = this._windowOnClickHandler.bind(this);
            this._staticDateFieldHandler = this._staticDateFieldHandler.bind(this);
            this.opened = false;
            this.disabled = false;
            this.$wrapper = this.$el.wrap('<div class="dropdown" />').parent();
            this.$label = $('<div class="dropdown__current-label" />');
            this.$options = $('<div class="dropdown__options" />');
            this.$link = $('<div class="dropdown__link" />');
            this.$el.addClass('dropdown__field');

            this.$el.before(this.$label);
            this.$el.before(this.$options);
            this.$el.before(this.$link);

            var tagName = this.element.tagName.toLowerCase();
            var elementType = String(this.element.type).toLowerCase();

            this.dropdownType = '';
            if (tagName === 'select') {
                this.dropdownType = 'select';
            } else if (tagName === 'input' && elementType === 'date') {
                this.dropdownType = 'date';
            }


            if ( this.dropdownType === 'select' ) {
                this._initSelect();
            } else if (this.dropdownType === 'date') {
                this._initDatepicker();
            }

            this.$wrapper.toggleClass('dropdown--mobile', $.browser.mobile);
            this.$wrapper.toggleClass('dropdown--desktop', !$.browser.mobile);

            if ( !$.browser.mobile ) {
                this.$link.on('click', this._linkClickHandler);
                $(window).on('click blur', this._windowOnClickHandler);
            }

        },

        // Select
        _initSelect: function () {
            var _this = this;
            this.placeholderText = this.$el.attr('placeholder') || 'Select';

            if ($.browser.mobile) {
                this._onSelectMobile = this._onSelectMobile.bind(this);
                this.$el.on('change', this._onSelectMobile);
            } else {
                this.isMultiple = this.$el.attr('multiple') === 'multiple';

                if (this.isMultiple) {
                    this.$options.on('change', 'input[type="checkbox"]', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        _this._onSelectMultipleHandler(this);
                    });

                    var $checkBulk = $('<div class="dropdown__check-bulk" />').appendTo(this.$options);

                    var toggleAllHandler = function (state, event) {
                        _this.$options.find('input[type="checkbox"]').each(function (i, item) {
                            $(this).prop('checked', state).change();
                        });
                    };

                    this.$checkAllLink = $('<a class="dropdown__check-all" />')
                        .text(this.settings.checkAll)
                        .on('click', toggleAllHandler.bind(this, true))
                        .appendTo($checkBulk);

                    this.$unCheckAllLink = $('<a class="dropdown__uncheck-all" />')
                        .text(this.settings.unCheckAll)
                        .on('click', toggleAllHandler.bind(this, false))
                        .appendTo($checkBulk);

                } else {
                    this.$options.on('click', '.dropdown__option', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        _this._onSelectHandler(this);
                    });
                }

                this.$el.children().each(function (i, child) {
                    var tagName = child.tagName.toLowerCase();
                    if (tagName === 'option') {
                        this._renderOption(child);
                    } else {
                        this._renderOptgroup(child);
                    }
                }.bind(this));
            }
            this._setCurrentLabel();
        },
        _renderOption: function (optionElement, $container) {
            var _this = this;
            var $option = $('<div class="dropdown__option" />');
            var $optionElement = $(optionElement);

            if (this.isMultiple) {
                $option.addClass('dropdown__option--multiple');
                var labelText = $optionElement.text();
                var $label = $('<label class="dropdown__option-label" />').attr('title', labelText);
                var $checkBox = $('<input class="dropdown__checkbox" type="checkbox" />')
                    .data('optionElement', $optionElement)
                    .data('fakeOption', $option)
                    .appendTo($label);

                if ($optionElement.is(':selected')) {
                    $checkBox.attr('checked', true);
                    $option.addClass('dropdown__option--active');
                }

                $('<span  />')
                    .text(labelText)
                    .appendTo($label);

                $option.append($label);
            } else {
                if ($optionElement.is(':selected')) {
                    $option.addClass('dropdown__option--active');
                }
                $option
                    .attr('title', $optionElement.text())
                    .text($optionElement.text())
                    .data('optionElement', $optionElement)
            }
            if ($container instanceof $) {
                $container.append($option);
            } else {
                this.$options.append($option);
            }
        },
        _renderOptgroup: function (optgroupElement) {
            var $optionElement = $(optgroupElement);
            var $optgroup = $('<div class="dropdown__optgroup" />');

            $('<div class="dropdown__optgroup-title" />')
                .text( $optionElement.attr('label') )
                .appendTo($optgroup);

            $optionElement.children().each(function (i, child) {
                this._renderOption(child, $optgroup);
            }.bind(this));

            this.$options.append($optgroup);
        },
        _onSelectHandler: function (fakeOption) {
            var $fakeOption = $(fakeOption);
            var $option = $fakeOption.data('optionElement');
            this.$options.find('.dropdown__option--active').removeClass('dropdown__option--active');
            $fakeOption.addClass('dropdown__option--active');
            $option.prop('selected', true);
            this.$el.change();
            this._setCurrentLabel();
            this.close();
        },
        _onSelectMultipleHandler: function (checkbox) {
            var $checkbox = $(checkbox);
            var $optionElement = $checkbox.data('optionElement');
            var $fakeOption = $checkbox.data('fakeOption');
            var isSelected = $checkbox.is(':checked');
            $optionElement.prop('selected', isSelected);
            this.$el.change();
            $fakeOption.toggleClass('dropdown__option--active', isSelected);
            this._setCurrentLabel();
        },
        _onSelectMobile: function () {
            this._setCurrentLabel();
        },
        _setCurrentLabel: function () {
            var text = '';
            if (this.isMultiple) {
                var val = this.$el.val();
                if (val && val.length === 1) {
                    text = this.$el.find('option:selected').text();
                } else if (val && val.length > 1) {
                    text = val.length + ' items';
                }
            } else {
                text = this.$el.find('option:selected').text();
            }
            if (!text) {
                this.$label.text(this.placeholderText);
                this.$label.addClass('dropdown__current-label--placeholder');
            } else {
                this.$label.removeClass('dropdown__current-label--placeholder');
                this.$label.text(text);
            }
        },

        // Datepicker
        _initDatepicker: function () {
            var _this = this;
            this.placeholderText = this.$el.attr('placeholder') || 'Pick date';

            if ($.browser.mobile) {
                this.$el.on('change', this._staticDateFieldHandler);
                this.$el.addClass('dropdown__field--date');
            } else {
                this.datePickerContainerId = 'DropDownDatePicker_' + Math.round(Math.random() * 1e15);
                this.$datePicker = $('<div />')
                    .attr('id', this.datePickerContainerId)
                    .appendTo(this.$options);

                Flatpickr.constructor.l10ns.default.firstDayOfWeek = 1;
                Flatpickr.constructor.l10ns.default.weekdays.shorthand = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];


                this.$label.text(this.placeholderText);
                this.$label.addClass('dropdown__current-label--placeholder');

                this.datePickerPlugin = Flatpickr.init('#' + this.datePickerContainerId, {
                    inline: true,
                    static: true,
                    nextArrow: '',
                    prevArrow: '',
                    onChange: function (date) {
                        var date = date[0];
                        _this.$label.removeClass('dropdown__current-label--placeholder');
                        _this.$label.text( date.toString("dd-MMM-yy") );
                        _this.$el.val( date.toString("yyyy-MM-dd") ).change();
                        _this.close();
                    },
                    onMonthChange: function () {
                        this.$options.find('[tabindex]').attr('tabindex', '-1');
                    }
                });
                this.$options.find('.numInput.cur-year,[tabindex]').attr('tabindex', '-1');
            }
        },
        _staticDateFieldHandler: function (event) {
            var value = $(event.target).val();
            var date, labelText;
            if (value) {
                date = new Date(value);
                labelText = date.toString('dd-MMM-yy');
                this.$label.text(labelText);
            }

        },
        _linkClickHandler: function () {
            if (this.disabled) {
                return;
            }
            if (this.opened) {
                this.close();
            } else {
                this.open();
            }
        },
        _windowOnClickHandler: function (event) {
            var $target = $(event.target);
            if ( $target.closest(this.$wrapper.get(0)).length === 0 ) {
                this.close();
            }
        },
        open: function () {
            this.opened = true;
            this.$wrapper.addClass('dropdown--active');
            var showToTop = $(window).height() - this.$el.offset().top < 257;
            this.$options.toggleClass('dropdown__options--top', showToTop);
            this.$options.show();
        },
        close: function () {
            this.opened = false;
            this.$wrapper.removeClass('dropdown--active');
            this.$options.hide();
        },
        toggleDisabled: function (state) {
            state = typeof state === 'boolean' ? state : !this.disabled;
            this.disabled = Boolean(state);
            this.$el.attr('disabled', state);
            this.$wrapper.toggleClass('dropdown--disabled', state);
        },
        enable: function () {
            this.toggleDisabled(false);
            return this;
        },
        disable: function () {
            this.toggleDisabled(true);
            return this;
        },
        destroy: function () {
            $(window).off('click focus', this._windowOnClickHandler);
            this.$el.off('change', this._staticDateFieldHandler);
            this.$el.off('change', this._onSelectMobile);
            this.$link.off('click').remove();
            this.$options.off('click').off('change').remove();
            this.datePickerPlugin && this.datePickerPlugin.destroy();
            this.$label.remove();
            this.$checkAllLink && this.$checkAllLink.off('click').remove();
            this.$unCheckAllLink && this.$unCheckAllLink.off('click').remove();
            this.$el.unwrap();
            this.datePickerPlugin && this.datePickerPlugin.destroy();
            $.removeData(this.element, 'plugin_dropdown');
        }
    });

    $.fn.dropdown = function( options ) {
        return this.map(function() {
            var plugin = new Plugin(this, options);
            if ( !$.data(this, 'plugin_dropdown') ) {
                $.data(this, 'plugin_dropdown', plugin);
            }
            return plugin;
        });
    };
});
