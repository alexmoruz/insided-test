define(['jquery'], function ($) {

    var $table = $('.js-user-table');
    var $checkboxes = $table.find('.js-user-table-checkbox');
    var $baseCheckbox = $table.find('.js-user-table-main-checkbox');
    var $actions = $table.find('.js-user-table-actions');

    $baseCheckbox.on('change', function () {
        var isCheched = $(this).prop('checked');
        $checkboxes.prop('checked', isCheched).change();
    });

    $checkboxes.on('change', function () {
        var $checkbox = $(this);
        var $parentRow = $checkbox.closest('.js-user-table__row');
        var val = $checkbox.prop('checked');
        $parentRow.toggleClass('is-user-table__row--selected', val);

        var existsChecked = Boolean($checkboxes.filter(':checked').length);
        $baseCheckbox.prop('checked', existsChecked);
        $actions.toggle(existsChecked);
    });

});
