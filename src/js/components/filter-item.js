define([
    'jquery',
    'components/dropdown'
], function ($) {

    var defaults = {
        onColumnTypeChange: $.noop,
        onRemove: $.noop
    };

    var rowTemplate = [
        '<div class="search-box__filter-row">',
            '<div class="search-box__filter-row-container" />',
            '<button class="button button--red button--disabled js-search-row-remove" disabled>',
                '<i class="icon icon--times icon--red"></i>',
            '</button>',
        '</div>',
    ].join('');

    var filterTypeSelectTemplate = [
        '<select class="js-search-filter-type" name="" placeholder="Select column">',
            '<option></option>',
            '<option value="comments">Comments</option>',
            '<option value="registration_date">Registration date</option>',
            '<option value="usergroup">Usergroup</option>',
        '</select>'
    ].join('');

    var numberComparatorTemplate = [
        '<select class="js-search-filter-number-comparator" name="">',
            '<option></option>',
            '<option value="between">Between</option>',
            '<option value="before">Before</option>',
            '<option value="after">After</option>',
            '<option value="on">On</option>',
        '</select>',
    ].join('');

    var dateComparatorTemplate = [
        '<select class="js-search-filter-date-comparator" name="">',
            '<option></option>',
            '<option value="before">Before</option>',
            '<option value="after">After</option>',
            '<option value="on">On</option>',
        '</select>',
    ].join('');

    var userGroupTemplate = [
        '<select class="js-search-filter-usergroup" name="" multiple="true" placeholder="Select one or more options">',
            '<optgroup label="Group 1">',
                '<option value="1">Option #1 for group 1</option>',
                '<option value="2">Option #2 for group 1</option>',
                '<option value="3">Option #3 for group 1</option>',
            '</optgroup>',
            '<optgroup label="Group 2">',
                '<option value="4">Forum</option>',
                '<option value="5">Ipsum</option>',
                '<option value="6">Dorol</option>',
            '</optgroup>',
        '</select>',
    ].join('');

    var dateDropdownTemplate = [
        '<input class="js-search-filter-date" type="date" />',
    ].join('');



    function FilterItem (options) {
        this.settings = $.extend({}, defaults, options);
        this._init();
    }

    $.extend(FilterItem.prototype, {
        _init: function () {
            var _this = this;
            this.$row = $(rowTemplate);

            this._filterTypeHandler = this._filterTypeHandler.bind(this);
            this._dateComparatorHandler = this._dateComparatorHandler.bind(this);
            this._commetsComparatorHandler = this._commetsComparatorHandler.bind(this);
            this._dateHandler = this._dateHandler.bind(this);
            this._usergroupHandler = this._usergroupHandler.bind(this);

            this.$container = this.$row.find('.search-box__filter-row-container');
            this.$remove = this.$row.find('.js-search-row-remove');

            // append first select
            this.$filterTypeSelect = this._createField(filterTypeSelectTemplate, this._filterTypeHandler);

            // append second default comparator select
            this.$numberComparatorSelect = this._createField(numberComparatorTemplate, this._commetsComparatorHandler);
            var plugin = this.$numberComparatorSelect.data('plugin_dropdown');
            plugin && plugin.disable();

            // destroy row
            this.$remove.on('click', function () {
                _this.destroy();
            });

            $('#searchCurrentFilters').append(this.$row);
        },
        _filterTypeHandler: function (event) {
            this.settings.onColumnTypeChange(event.target);
            var value = event.target.value;

            switch (value) {
                case 'comments':
                    this._hideAndDisable('.js-search-filter-date-comparator, .js-search-filter-number-comparator, .js-search-filter-date, .js-search-filter-usergroup');
                    this._showAndEnable(this.$numberComparatorSelect);
                    break;
                case 'registration_date':
                    this._hideAndDisable('.js-search-filter-number-comparator, .js-search-filter-usergroup');

                    if ( !this.$container.find('.js-search-filter-date-comparator').length ) {
                        this.$dateComparatorSelect = this._createField(dateComparatorTemplate, this._dateComparatorHandler);
                    } else {
                        this._showAndEnable(this.$dateComparatorSelect);
                    }

                    if ( !this.$container.find('.js-search-filter-date').length ) {
                        this.$dateSelect = this._createField(dateDropdownTemplate, this._dateHandler);
                        var plugin = this.$dateSelect.data('plugin_dropdown');
                        plugin && plugin.disable();
                    } else {
                        this._showAndEnable(this.$dateSelect);
                    }
                    break;
                case 'usergroup':
                    this._hideAndDisable('.js-search-filter-date-comparator, .js-search-filter-number-comparator, .js-search-filter-date');

                    if ( !this.$container.find('.js-search-filter-usergroup').length ) {
                        this.$dateUsergroupSelect = this._createField(userGroupTemplate, this._usergroupHandler);
                    } else {
                        this._showAndEnable(this.$dateUsergroupSelect);
                        // plugin = this.$dateUsergroupSelect.data('plugin_dropdown');
                        // plugin && plugin.enable().showDropdown();
                    }
                    break;
            };
        },
        _createField: function (template, handler) {
            var $res = $(template).appendTo(this.$container);
            if (typeof handler === 'function') {
                $res.on('change', handler);
            }
            $res.wrap('<div class="search-box__filter-field" />');
            $res.dropdown();
            return $res;
        },
        _hideAndDisable: function (selector) {
            this.$row.find(selector).each(function () {
                $(this).closest('.search-box__filter-field').hide();
                var plugin = $(this).data('plugin_dropdown');
                if (plugin) {
                    plugin.disable();
                }
            });
        },
        _showAndEnable: function ($field) {
            $field.closest('.search-box__filter-field').show();
            var plugin = $field.data('plugin_dropdown');
            plugin && plugin.enable();

        },
        _dateComparatorHandler: function () {
            var plugin = this.$dateSelect.data('plugin_dropdown');
            plugin && plugin.enable();
        },
        _dateHandler: function () {
            this.enableRemoveButton();
        },
        _commetsComparatorHandler: function () {
            this.enableRemoveButton();
        },
        _usergroupHandler: function () {
            this.enableRemoveButton();
        },
        enableRemoveButton: function () {
            this.$remove
                .removeClass('button--disabled')
                .attr('disabled', false);
        },
        destroy: function () {
            this.settings.onRemove();
            this.$remove.off('click');
            this.$filterTypeSelect && this.$filterTypeSelect.off('change');
            this.$numberComparatorSelect && this.$numberComparatorSelect.off('change');
            this.$dateComparatorSelect && this.$dateComparatorSelect.off('change');
            this.$dateSelect && this.$dateSelect.off('change', this._dateHandler)
            this.$row.remove();
        }
    });

    return FilterItem;
});
